# EProcess

A lightweight library that help to process, slice and filter a lot of data. 


# Authors

- Elio Rincón - eliosf27@gmail.com


# License

eprocess is released under the MIT License. See LICENSE for more information.